---
layout: post
title:  "Terraform - InvalidID on aws_eip"
date:   2020-04-28 12:39:23 -0500
categories: terraform aws
---
I was trying to apply a terraform template today and ran into this error:

```
module.network.aws_eip.eip_nat_az2: Creating...

Error: error adding tags: error tagging resource (18.214.131.208): InvalidID: The ID '18.214.131.208' is not valid
        status code: 400, request id: 49aab8a6-8c26-4327-90a5-389a2e5962bd

  on ../modules/network/main.tf line 104, in resource "aws_eip" "eip_nat_az1":
 104: resource "aws_eip" "eip_nat_az1" {



Error: error adding tags: error tagging resource (100.24.169.116): InvalidID: The ID '100.24.169.116' is not valid
        status code: 400, request id: 212e2891-6500-4150-94e1-76f28603a2b2

  on ../modules/network/main.tf line 111, in resource "aws_eip" "eip_nat_az2":
 111: resource "aws_eip" "eip_nat_az2" {



Error: error adding tags: error tagging resource (52.206.41.2): InvalidID: The ID '52.206.41.2' is not valid
        status code: 400, request id: 38dbad9b-8985-428d-ae67-064fdd4f0f93
```

NOTE: These IPs are just random from the AWS pool and don't really point to anything useful.

Searching for this issue yielded no results, so I figured I should post the solution.

It turns out my module had defined eips like so:

```hcl
resource "aws_eip" "eip_nat_az1" {
    tags {
        Name = "foo"
        Environment = "bar"
    }
}
```

When I examined the created IP addresses in the AWS console I saw that they were "Classic" EIPs and not VPC.

I had forgotten to specify `vpc = true`. The module now looks like this:

```hcl
resource "aws_eip" "eip_nat_az1" {
    vpc = true
    tags {
        Name = "foo"
        Environment = "bar"
    }
}
```

Now `terraform apply` runs cleanly!
