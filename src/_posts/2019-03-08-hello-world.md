---
layout: post
title:  "Hello, World!"
date:   2019-03-08 14:54:23 -0600
categories: test update
---

This appears to be the best way to create a simple markdown site. I plan on putting my notes here and any explorations that I think deserve a blog post.

Basically this is a playground. Hopefully someone can find it useful.

PEACE