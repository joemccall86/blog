---
layout: post
title:  "DataBinding and GORM objects"
date:   2019-02-20 14:54:23 -0600
categories: grails groovy gorm
---
In Groovy, you are able to do this:

```groovy
class Widget {
  String name
}

def widget = new Widget(name: '')
assert widget.name == ''
```

When this widget is a GORM object (in a Grails application), the default configuration doesn’t allow for this:

```groovy
// Inside grails-app/domain
class PendingDeletion {
  String name
  String path
}

// Elsewhere in code
def pendingDeletion = new PendingDeletion(name: 'foo', path: '')
assert pendingDeletion.path == '' // Fails; pendingDeletion.path is null
```

This is because the default configuration instructs the Grails data-binder to convert empty strings to null [^1].

I’m not exactly sure why this is the default. In order to make the above snippet work in your Grails application, specify the following in application.yml:

```yml
grails:
  databinding:
    convertEmptyStringsToNull: false
```

This allows the GORM map constructor to behave the same as a normal POGO map constructor.

Alternatively, just do not use the GORM map constructor when the field values can be null.

[^1]: http://docs.grails.org/3.3.9/ref/Constraints/nullable.html

 
